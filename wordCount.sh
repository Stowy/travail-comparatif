#!/bin/bash

NBR_WORDS=2400
MIN_WORDS=$(echo '2400 - 2400 * 0.1' | bc -l)
MAX_WORDS=$(echo '2400 + 2400 * 0.1' | bc -l)

echo "Nombres de mots qu'il faut : $NBR_WORDS [${MIN_WORDS} - ${MAX_WORDS}]"

echo "detex : "
detex travail-comparatif.tex | awk NF | wc -w

echo "pdf to text : "
pdftotext "travail-comparatif.pdf" && cat "travail-comparatif.txt" | awk NF | wc -w && rm "travail-comparatif.txt"
